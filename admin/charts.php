<?php include('includes/header.php') ?>
<?php


if($session->role_id !=='1' && $session->role_id !=='2'){
    $session->message("You don't have access to that page");
    check_role($session->role_id);

}
?>


<body id="page-top">

<?php include('includes/top_nav.php') ?>



  <div id="wrapper">

    <!-- Sidebar -->
<?php include('includes/sidebar.php') ?>

    <div id="content-wrapper">
        <div class="container-fluid">

        <!-- Breadcrumbs-->
        <ol class="breadcrumb">
          <li class="breadcrumb-item">
            <a href="#">Dashboard</a>
          </li>
          <li class="breadcrumb-item active">Charts</li>
        </ol>
        <h6 class="bg-danger"><?php echo $message; ?></h6>

      </div>

      <div class="chartdiv">
        <?php include("js/barchart/jsindex.php"); ?>
      </div>

      <!-- /.container-fluid -->
<?php include('includes/footer.php') ?>
      <!-- Sticky Footer -->
