<?php
include ('includes/header.php');
if(!$session->is_singed_in()) {redirect("login.php");}
$teacher_list = User::list_teachers();
?>
<body id="page-top">
    <!-- Top Nav -->
    <!-- Sidebar -->

    <?php
            if($session->role_id == 4){
                include 'includes/top_nav_for_parents.php';
                echo '<div id="wrapper">';
                include 'includes/sidebar_for_parents.php';
            }elseif($session->role_id == 3){
                include 'includes/top_nav_for_teachers.php';
                echo '<div id="wrapper">';
                include 'includes/sidebar_for_teachers.php';
            }else{
                include 'includes/top_nav.php';
                echo '<div id="wrapper">';
                include 'includes/sidebar.php';
            }
        if(isset($_GET['notif_id'])){
            $notification = Notification::find_by_id($_GET['notif_id']);
        }elseif(isset($_GET['open_door_id'])){
            $open_door = Open_door::find_by_id($_GET['open_door_id']);
          // var_dump($open_door);
        }


    ?>
            <div id="content-wrapper">
                <div class="container-fluid">
            <div class="container">

                        <?php
                            if(isset($notification)){
                                echo "<h1><i>".$notification->title."</i></h1>";
                            }elseif(isset($open_door)){
                                $term_id = $open_door->term_id;
                                $term_info = Terms::find_by_id($term_id);
                                $host = User::find_by_id($_GET['host_id']);
                                Open_door::info_of_confirmation($open_door->confirm, $host->first_name,$host->last_name, $term_info->term, $open_door->date);

                            }
                        ?>

                <div class="dropdown-divider"></div>

                <blockquote>
                    <p>
                        <?php
                            if(isset($notification)){
                                echo $notification->text;
                            }

                        ?>
                    </p>
                </blockquote>
            </div>

                    <?php  include ('includes/footer.php') ?>
