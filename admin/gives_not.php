<?php
include ('includes/header.php');
if(isset($_POST['view'])) {

    if($_POST["view"] != '') {
        $update = Notifications::update_notification();
    }
    $notifications = Notifications::show_notification();
    $output = '';
    if(sizeof($notifications)==0){
        $output .= '<li><a href="#" class="text-bold text-italic">No Notif Found</a></li>';
    }else{
        foreach($notifications as $n){
            $output .= '<li><a href=""><strong>' . $n->notif_title . '</strong><br><small><em>' . $n->notif_text . '</em></small></a></li>';
        }
    }
   $count = Notifications::count_unseen();
    $data = array(
        'notification' => $output,
        'unseen_notification'  => $count
    );
   //print_r($data);

    echo json_encode($data);
}