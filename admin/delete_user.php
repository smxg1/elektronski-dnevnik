<?php 
include('includes/header.php'); 
if(!$session->is_singed_in()) {redirect("login.php"); } 
if($session->role_id !=='1'){
  $session->message("You don't have access to that page");
  check_role($session->role_id);

}



if(empty($_GET['id'])){

    redirect('users.php');
}

$user = User::find_by_id($_GET['id']);

if($user){

	$user->id = $_GET['id'];
	$user->delete();
	$session->message("The user {$user->first_name} {$user->last_name} has been deleted");
	redirect('view_users.php');

} else {

	redirect('view_users.php');

}









 ?>