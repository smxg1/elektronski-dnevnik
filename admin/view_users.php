<?php include ('includes/header.php')?>
<?php if(!$session->is_singed_in()) {redirect("login.php");}?>
<?php

if($session->role_id !=='1'){
  $session->message("You don't have access to that page");
  check_role($session->role_id);

}

 ?>
<!-- Top Nav -->
<?php include ('includes/top_nav.php')?>

<?php


$users = User::join_user_role();


$students = Student::find_all();



 ?>

  <div id="wrapper">

    <!-- Sidebar -->
      <?php include ('includes/sidebar.php')?>

    <div id="content-wrapper">
    	<div class="container-fluid">

       <ol class="breadcrumb">
          <li class="breadcrumb-item">
            <a href="index.php">Admin Dashboard</a>
          </li>
          <li class="breadcrumb-item active">View All Users</li>
        </ol>
         <p class="bg-success">
              <?php echo $message; ?>
         </p>

        <!-- DataTables Example -->
        <div class="card mb-3">
          <div class="card-header">
            <i class="fas fa-table"></i>
            Data Table Example</div>
          <div class="card-body">
            <div class="table-responsive">
              <table class="table table-bordered" id="dataTable" width="100%" cellspacing="0">
                <thead>
                  <tr>
                    <th>Id</th>
                    <th>First Name</th>
                    <th>Last Name</th>
                    <th>Username</th>
                    <th>User Role</th>
                    <th>Edit/Delete</th>
                  </tr>
                </thead>
                <tfoot>
                  <tr>
                    <th>Id</th>
                    <th>First Name</th>
                    <th>Last Name</th>
                    <th>Username</th>
                    <th>User Role</th>
                    <th>Edit/Delete</th>
                  </tr>
                </tfoot>
                <tbody>

<?php  foreach($users as $user) : ?>

                  <tr>
                    <td><?php echo $user->id; ?></td>
                    <td><?php echo $user->first_name; ?></td>
                    <td><?php echo $user->last_name; ?></td>
                    <td><?php echo $user->username; ?></td>
                      <td><?php echo $user->role_name; ?></td>
                    <td><a href="edit_user.php?id=<?php echo $user->id; ?>">Edit</a> /
                    	<a href="delete_user.php?id=<?php echo $user->id; ?>">Delete</a></td>


                  </tr>

<?php endforeach; ?>

                </tbody>
              </table>
            </div>
          </div>
          <div class="card-footer small text-muted">Updated yesterday at 11:59 PM</div>
        </div>

      <!-- /.container-fluid -->

      <!-- Sticky Footer -->

<?php  include ('includes/footer.php') ?>
