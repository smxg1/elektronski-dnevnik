<?php require_once("includes/header.php"); ?>

<?php 

if($session->is_singed_in()){
  redirect("index.php");
}

if(isset($_POST['submit'])){
  $username = trim($_POST['username']);
  $password = trim($_POST['password']);


// Method to check database user

$user_found = User::verify_user($username,$password);

  if($user_found){
    $session->login($user_found);
    redirect("index.php");



  } else {
    $the_message = "Your username or password is incorrect";
  }

} else {
  $username = "";
  $password = "";
  $the_message = "";
}


?>



<body class="bg-dark">

  <div class="container">
    <div class="card card-login mx-auto mt-5">
      <div class="card-header">Login</div>
      <div class="card-body">
        <h4 class="bg-danger"><?php echo $the_message; ?></h4>

        <form action="" method="POST">
          <div class="form-group">
            <div class="form-group">
               <label for="username">Username</label>
              <input type="text"  name='username' class="form-control"  value="<?php echo htmlentities($username); ?>">

            </div>
          </div>
          <div class="forrm-group">
            <div class="form-group">
               <label for="password">Password</label>
              <input type="password" name='password' class="form-control" value="<?php echo htmlentities($password); ?>">

            </div>
          </div>

          <input type="submit" name="submit" class="btn btn-primary btn-block" value="Login">

        </form>

        <div class="text-center">
          <a class="d-block small mt-3" href="register.html">Register an Account</a>
          <!-- <a class="d-block small" href="forgot-password.html">Forgot Password?</a> -->
        </div>
      </div>
    </div>
  </div>

  <!-- Bootstrap core JavaScript-->
  <script src="vendor/jquery/jquery.min.js"></script>
  <script src="vendor/bootstrap/js/bootstrap.bundle.min.js"></script>

  <!-- Core plugin JavaScript-->
  <script src="vendor/jquery-easing/jquery.easing.min.js"></script>

</body>

</html>
