<?php
include ('includes/header.php');
if(isset( $_GET['teacher_id'])){
    $teacher_id=$_GET['teacher_id'];
    $user = User::find_by_id($teacher_id);
    $list_of_terms = Terms::list_of_terms($teacher_id);
}
    if(!$session->is_singed_in()) {redirect("login.php");}
        $grades = Grade::show_grades_to_parent($session->user_id);
        $teacher_list = User::list_teachers();


?>
    <body id="page-top">
    <!-- Top Nav -->
    <!-- Sidebar -->

    <?php
            if($session->role_id == 4){
                include 'includes/top_nav_for_parents.php';
                echo '<div id="wrapper">';
                include 'includes/sidebar_for_parents.php';
            }else{
                include ('includes/sidebar.php');
                echo '<div id="wrapper">';
                include ('includes/top_nav.php');
            }
            $apoint = new Open_door;
            if(isset($_GET['submit_term'])){
                if($apoint){
                    if(isset($_GET['radiobutton']) and isset($_GET['date']) and $_GET['date']!=''){

                        $apoint->visitor = $session->user_id;
                        $apoint->host = $_GET['teacher_id'];
                        $apoint->term_id = $_GET['radiobutton'];
                        $term_info = Terms::find_by_id($apoint->term_id);
                        $apoint->confirm = 'Pending';
                        $apoint->date = $_GET['date'];
                        $apoint->status = '0';
                        $apoint->save();
                        $session->message("<br><h2 style=\"color: red\">Zakazali ste kod učitelja<i> {$user->first_name} {$user->last_name}</i>
                                                                            posetu u terminu:<br><i> {$term_info->term},</i> dana: {$_GET['date']}. 
                                                                            Sačekajte potvrdu...</h2>");
                        redirect($_SERVER['HTTP_REFERER']);
                        }else{
                            $session->message("<i><h3 style=\"color: deeppink\">Niste izabrali datum...</h3></i>");
                            redirect($_SERVER['HTTP_REFERER']);
                        }
                    }
                }
    ?>

    <div id="content-wrapper">
        <div class="container-fluid">
            <!-- Breadcrumbs-->
            <ol class="breadcrumb">
                <li class="breadcrumb-item">
                    <a href="index.php"><?php     echo $grades[0]->student_name;  ?></a>
                </li>
                <li class="breadcrumb-item active"></li>
            </ol>
            <h3>Raspoloživi termini kod učitelja:  <?php  echo $user->first_name."  ".$user->last_name; ?> </h3><br>
            <div class="container">
            <form action="<?php echo htmlspecialchars($_SERVER["PHP_SELF"]);?>" method="GET">
                        <?php
                        foreach($list_of_terms as $term){
                            echo "<div class=\"form-check\">
                                    <label class='form-check-label' for=".$term->term_id."></label>";
                            echo "<input type='radio' class='form-check-input' id='".$term->term_id."' name='radiobutton' value='".$term->term_id."'><span>".$term->term. "
                                    </span></div><br>";
                        }

                        ?>
                    <input type='hidden' name='teacher_id' value="<?php echo $teacher_id; ?>">


                    <label for="date">Izaberite željeni datum: </label>
                <input type="date" name="date" id="date">
                    <!-- <input name="date" class="datepicker">-->


                <button type="submit" name="submit_term" class="btn btn-primary">Potvrdi izbor</button>

            </form>
            </div>


            <script>


                    $('.datepicker').pickadate();

            </script>
<?php if($message) echo $message;?>

    <!-- /.container-fluid -->
    <!-- Sticky Footer -->
<?php  include ('includes/footer.php') ?>

