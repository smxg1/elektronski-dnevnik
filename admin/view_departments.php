<?php include ('includes/header.php')?>
<?php if(!$session->is_singed_in()) {redirect("login.php");}?>
<?php
if($session->role_id !=='1'){

    check_role($session->role_id);

}


$departments = Departmant::find_all_departments();

?>

    <body id="page-top">
    <!-- Top Nav -->
<?php include ('includes/top_nav.php')?>

<div id="wrapper">

    <!-- Sidebar -->
<?php include ('includes/sidebar.php')?>

    <div id="content-wrapper">

        <div class="container-fluid">


            <!-- Breadcrumbs-->

            <ol class="breadcrumb">
                <li class="breadcrumb-item">
                    <a href="index.php">Admin Dashboard</a>
                </li>
                <li class="breadcrumb-item active">Overview</li>
            </ol>

            <p id="demo"></p>


            <table class="table table-bordered col-md-4">
                <thead>
                <tr>
                    <th>Class Name</th>
                    <th>Teacher Name</th>
                    <th>Schedule</th>
                </tr>
                </thead>
                <tbody>
                <?php foreach ($departments as $department) : {?>
                <tr>
                    <td><?php echo $department->name; ?></td>
                    <td><?php echo $department->first_name . " "  . $department->last_name; ?></td>
                    <td><a href="make_schedule.php?c_id=<?php echo $department->id;?>">Make Schedule</a></td>

                </tr>
                <?php } endforeach;?>

                </tbody>
            </table>

<!--    <div class="container">-->
<!--        -->
<!---->
<!--    </div>-->

    <!-- /.container-fluid -->

    <!-- Sticky Footer -->

<?php  include ('includes/footer.php') ?>