<?php include ('includes/header.php')?>
<?php if(!$session->is_singed_in()) {redirect("login.php");}?>
<?php
if($session->role_id !=='1'){

    check_role($session->role_id);

}
if(!isset($_GET['c_id'])){
    redirect('view_departments.php');
}

$days = new Days();
$c_id = $_GET['c_id'];
$new_schedule = new Schedule();

if(isset($_POST['submit'])){

    if($new_schedule){
        $new_schedule->id = $_GET['sch_id'];
        $new_schedule->day_id = $_GET['day_id'];
        $new_schedule->period_id = $_GET['p_id'];
        $new_schedule->subject_id = $_POST['subject_id'];
        $new_schedule->class_id = $_GET['c_id'];
        $new_schedule->save();

    }


}

if(isset($_POST['create'])){
    if($new_schedule){
        $new_schedule->day_id = $_POST['day_id'];
        $new_schedule->period_id = $_POST['period_id'];
        $new_schedule->subject_id = $_POST['subject_id'];
        $new_schedule->class_id = $_GET['c_id'];
        $new_schedule->save();

    }

}





?>

    <body id="page-top">
    <!-- Top Nav -->
<?php include ('includes/top_nav.php')?>

<div id="wrapper">

    <!-- Sidebar -->
<?php include ('includes/sidebar.php')?>

    <div id="content-wrapper">

    <div class="container-fluid">


    <!-- Breadcrumbs-->

    <ol class="breadcrumb">
        <li class="breadcrumb-item">
            <a href="index.php">Admin Dashboard</a>
        </li>
        <li class="breadcrumb-item active">Overview</li>
    </ol>

    <div class="row">

        <div class="col-md-10" ">
        <div class="col-md-2 raspored">
            <table class="table table-bordered">
                <thead>
                <tr>
                    <th>Ponedeljak</a></th>
                </tr>
                </thead>
                <tbody>
                <?php
                $schedules = Schedule::show_schedule_by_day($c_id,$days->ponedeljak);
                foreach ($schedules as $schedule) : ?>
                    <tr>

                        <td><a href="make_schedule.php?c_id=<?php echo $c_id;?>&day_id=<?php echo $days->ponedeljak;?>&p_id=<?php echo $schedule->period_number;?>&sch_id=<?php echo $schedule->id;?>"><?php echo $schedule->name;?></a></td>

                    </tr>
                <?php endforeach; ?>
                <?php
                $count_periods = Schedule::check_periods($c_id,$days->ponedeljak);

                if(count($count_periods) < 6) { ?>
                    <td><a class="btn btn-primary" href="schedule_form.php?c_id=<?php echo $c_id;?>&day_id=<?php echo $days->ponedeljak;?>">Create</a></td>
                <?php } ?>
                </tbody>
            </table>
        </div>
            <div class="col-md-2 raspored">
                <table class="table table-bordered">
                    <thead>
                    <tr>
                        <th>Utorak</a></th>
                    </tr>
                    </thead>
                    <tbody>
                    <?php
                    $schedules1 = Schedule::show_schedule_by_day($c_id,$days->utorak);
                    foreach ($schedules1 as $schedule) : ?>
                        <tr>

                            <td><a href="make_schedule.php?c_id=<?php echo $c_id;?>&day_id=<?php echo $days->utorak;?>&p_id=<?php echo $schedule->period_number;?>&sch_id=<?php echo $schedule->id;?>"><?php echo $schedule->name;?></a></td>

                        </tr>
                    <?php endforeach; ?>
                    <?php
                    $count_periods2 = Schedule::check_periods($c_id,$days->utorak);

                    if(count($count_periods2) < 6) { ?>
                        <td><a class="btn btn-primary" href="schedule_form.php?c_id=<?php echo $c_id;?>&day_id=<?php echo $days->utorak;?>">Create</a></td>
                    <?php } ?>
                    </tbody>
                </table>
            </div>
            <div class="col-md-2 raspored">
                <table class="table table-bordered">
                    <thead>
                    <tr>
                        <th>Sreda</a></th>
                    </tr>
                    </thead>
                    <tbody>
                    <?php
                    $schedules2 = Schedule::show_schedule_by_day($c_id,$days->sreda);
                    foreach ($schedules2 as $schedule) : ?>
                        <tr>

                            <td><a href="make_schedule.php?c_id=<?php echo $c_id;?>&day_id=<?php echo $days->sreda;?>&p_id=<?php echo $schedule->period_number;?>&sch_id=<?php echo $schedule->id;?>"><?php echo $schedule->name;?></a></td>

                        </tr>
                    <?php endforeach; ?>
                    <?php
                    $count_periods3 = Schedule::check_periods($c_id,$days->sreda);
                    if(count($count_periods3) < 6) { ?>
                        <td><a class="btn btn-primary" href="schedule_form.php?c_id=<?php echo $c_id;?>&day_id=<?php echo $days->sreda;?>">Create</a></td>
                    <?php } ?>
                    </tbody>
                </table>
            </div>
            <div class="col-md-2 raspored">
                <table class="table table-bordered">
                    <thead>
                    <tr>
                        <th>Cetvrtak</a></th>
                    </tr>
                    </thead>
                    <tbody>
                    <?php
                    $schedules3 = Schedule::show_schedule_by_day($c_id,$days->cetvrtak);
                    foreach ($schedules3 as $schedule) : ?>
                        <tr>

                            <td><a href="make_schedule.php?c_id=<?php echo $c_id;?>&day_id=<?php echo $days->cetvrtak;?>&p_id=<?php echo $schedule->period_number;?>&sch_id=<?php echo $schedule->id;?>"><?php echo $schedule->name;?></a></td>

                        </tr>


                    <?php  endforeach; ?>

                    <?php
                    $count_periods4 = Schedule::check_periods($c_id,$days->cetvrtak);

                    if(count($count_periods4) < 6) { ?>
                        <td><a class="btn btn-primary" href="schedule_form.php?c_id=<?php echo $c_id;?>&day_id=<?php echo $days->cetvrtak;?>">Create</a></td>
                    <?php } ?>

                    </tbody>
                </table>
            </div>
            <div class="col-md-2 raspored">
                <table class="table table-bordered">
                    <thead>
                        <tr>
                         <th>Petak</a></th>
                        </tr>
                    </thead>
                    <tbody>
                    <?php
                    $schedules4 = Schedule::show_schedule_by_day($c_id,$days->petak);
                    foreach ($schedules4 as $schedule) : ?>
                        <tr>

                            <td><a href="make_schedule.php?c_id=<?php echo $c_id;?>&day_id=<?php echo $days->petak;?>&p_id=<?php echo $schedule->period_number;?>&sch_id=<?php echo $schedule->id;?>"><?php echo $schedule->name;?></a></td>

                        </tr>

                    <?php  endforeach;?>


                    <?php
                    $count_periods5 = Schedule::check_periods($c_id,$days->petak);

                    if(count($count_periods5) < 6) { ?>
                        <td><a class="btn btn-primary" href="schedule_form.php?c_id=<?php echo $c_id;?>&day_id=<?php echo $days->petak;?>">Create</a></td>
                    <?php } ?>




                    </tbody>
                </table>
            </div>
        </div>
<!--        end-->



    </div>
    <div>

        <div>
            <div>
                <form action="" method="POST">
                    <div class="form-gorup">
                        <?php
                        $subjects = Subject::find_all();

                        if(isset($_GET['c_id']) && isset($_GET['day_id']) && isset($_GET['p_id'])){

                            echo "<label>Subject</label><br>";
                            echo "<select multiple class='form-group' style='height: 150px;width: 200px;  '  name='subject_id'><br>";

                            foreach ($subjects as $subject){
                                echo "<option value=".$subject->id .">" .$subject->name . "</option>";
                            }
                            echo "</select><br>";

                            echo "<div class='form-gorup'>";
                            echo "<input type='submit' name='submit' class='btn btn-primary' value='Change'><br>";
                            echo "</div>";

                        }

                        ?>

                    </div>

                </form>
            </div>
        </div>
    </div>





    <div>







        <script>


             // $("#show_form4").click(function(){
             //     $("#schedule").toggle(500);
             // });

            // if($("#show_form4").click(function(){
            //
            //     $("#schedule").toggle(500);
            // }));
            //
            //  if($("#show_form5").click(function(){
            //
            //      $("#schedule").toggle(500);
            //  }));

            
        </script>








    <!-- /.container-fluid -->

    <!-- Sticky Footer -->



<?php  include ('includes/footer.php') ?>