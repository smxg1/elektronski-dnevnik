<?php include ('includes/header.php');
if(!$session->is_singed_in()) {redirect("login.php");}
if($session->role_id !=='3'){
    $session->message("You don't have access to that page");
    check_role($session->role_id);
}
?>
<?php include ('includes/top_nav_for_teachers.php');
$student_id = $_GET['id'];
if( isset($_GET['subject_id']) and isset($_GET['sum']) and $_GET['sum'] != 0 ){
        $subject_id = $_GET['subject_id'];
        $sum = $_GET['sum'];
        $student = Student::find_by_id($student_id);
        $subject = Subject::find_by_id($subject_id);
        $exists_final_grade = Final_grade::find_by_student_class_id($student_id, $subject_id);
        Final_grade::insert_grade($subject_id ,$student_id ,$sum);
        redirect($_SERVER['HTTP_REFERER']);
    }else{
        $session->message("<p style='color:red'>Ne mozete zakljuciti NULA</p>");
        redirect($_SERVER['HTTP_REFERER']);
    }

?>