<?php include ('includes/header.php')?>
<?php if(!$session->is_singed_in()) {redirect("login.php");}

if($session->role_id !=='1'){
  $session->message("You don't have access to that page");
  check_role($session->role_id);

}
?>

<!-- Top Nav -->
<?php include ('includes/top_nav.php')?>


<?php

if(empty($_GET['id'])){

    redirect('view_users.php');
}

$user = User::find_by_id($_GET['id']);


if(isset($_POST['update'])){

//	$user->id = $_GET['id'];
	$user->first_name = $_POST['first_name'];
	$user->last_name = $_POST['last_name'];
	$user->username = $_POST['username'];				// Ovo mora jos da se sredi!!!
	$user->password = $_POST['password'];
	$user->role_id = $_POST['role_id'];
	$user->save();
	$session->message("The user {$user->first_name} {$user->last_name} has been updated");
	redirect('view_users.php');

}



 ?>

  <div id="wrapper">

    <!-- Sidebar -->
      <?php include ('includes/sidebar.php')?>

    <div id="content-wrapper">
      <div class="container-fluid">


        <!-- Breadcrumbs-->

        <ol class="breadcrumb">
          <li class="breadcrumb-item">
            <a href="index.php">Admin Dashboard</a>
          </li>
          <li class="breadcrumb-item active">Add User</li>
        </ol>

    <div class="col-md-6">
        <form action="" method="POST">
          <div class="form-group">
            <label for="first_name">First Name: </label>
            <input type="text" name="first_name" class="form-control" value="<?php echo $user->first_name; ?>">
          </div>
          <div class="form-group">
            <label for="lastname">Last Name: </label>
            <input type="text" name="last_name" class="form-control" value="<?php echo $user->last_name; ?>">
          </div>
           <div class="form-group">
            <label for="username">Username: </label>
            <input type="text" name="username" class="form-control" value="<?php echo $user->username; ?>">
          </div>
           <div class="form-group">
            <label for="password">Password: </label>
            <input type="password" name="password" class="form-control" value="<?php echo $user->password; ?>">
          </div>
          <div class="form-gorup">
            <label for="role_id">User Role</label>
            <select name="role_id">
              <option value="0">Choose Option</option>
              <option value="1">Administrator</option>
              <option value="2">Director</option>
              <option value="3">Teacher</option>
              <option value="4">Parent</option>
            </select>
          </div>
          <div class="form-gorup">
            <input type="submit" name="update" value="Update" class="btn btn-primary">
          </div>
   </div>





        </form>




      <!-- /.container-fluid -->

      <!-- Sticky Footer -->

<?php  include ('includes/footer.php') ?>
