<?php include ('includes/header.php')?>
<?php if(!$session->is_singed_in()) {redirect("login.php");}?>
<?php
if($session->role_id !=='3'){

    check_role($session->role_id);

}

if(!isset($_GET['c_id'])){
    redirect('view_students.php');
}


$days = new Days();
$c_id = $_GET['c_id'];





?>

    <body id="page-top">
    <!-- Top Nav -->
<?php include ('includes/top_nav_for_teachers.php')?>

<div id="wrapper">

    <!-- Sidebar -->
<?php include ('includes/sidebar_for_teachers.php')?>

    <div id="content-wrapper">

    <div class="container-fluid">


        <!-- Breadcrumbs-->

        <ol class="breadcrumb">
            <li class="breadcrumb-item">
                <a href="#">Raspored</a>
            </li>
            <li class="breadcrumb-item active">Pregled</li>
        </ol>

        <div class="row">

            <div class="col-md-10" ">
            <div class="col-md-2 raspored">
                <table class="table table-bordered">
                    <thead>
                    <tr>
                        <th>Ponedeljak</a></th>
                    </tr>
                    </thead>
                    <tbody>
                    <?php
                    $schedules = Schedule::show_schedule_by_day($c_id,$days->ponedeljak);
                    foreach ($schedules as $schedule) : ?>
                        <tr>

                            <td><?php echo $schedule->name;?></td>

                        </tr>
                    <?php endforeach; ?>

                    </tbody>
                </table>
            </div>
            <div class="col-md-2 raspored">
                <table class="table table-bordered">
                    <thead>
                    <tr>
                        <th>Utorak</a></th>
                    </tr>
                    </thead>
                    <tbody>
                    <?php
                    $schedules1 = Schedule::show_schedule_by_day($c_id,$days->utorak);
                    foreach ($schedules1 as $schedule) : ?>
                        <tr>

                            <td><?php echo $schedule->name;?></td>

                        </tr>
                    <?php endforeach; ?>

                    </tbody>
                </table>
            </div>
            <div class="col-md-2 raspored">
                <table class="table table-bordered">
                    <thead>
                    <tr>
                        <th>Sreda</a></th>
                    </tr>
                    </thead>
                    <tbody>
                    <?php
                    $schedules2 = Schedule::show_schedule_by_day($c_id,$days->sreda);
                    foreach ($schedules2 as $schedule) : ?>
                        <tr>

                            <td><?php echo $schedule->name;?></td>

                        </tr>
                    <?php endforeach; ?>

                    </tbody>
                </table>
            </div>
            <div class="col-md-2 raspored">
                <table class="table table-bordered">
                    <thead>
                    <tr>
                        <th>Cetvrtak</a></th>
                    </tr>
                    </thead>
                    <tbody>
                    <?php
                    $schedules3 = Schedule::show_schedule_by_day($c_id,$days->cetvrtak);
                    foreach ($schedules3 as $schedule) : ?>
                        <tr>

                            <td><?php echo $schedule->name;?></td>

                        </tr>


                    <?php  endforeach; ?>

                    </tbody>
                </table>
            </div>
            <div class="col-md-2 raspored">
                <table class="table table-bordered">
                    <thead>
                    <tr>
                        <th>Petak</a></th>
                    </tr>
                    </thead>
                    <tbody>
                    <?php
                    $schedules4 = Schedule::show_schedule_by_day($c_id,$days->petak);
                    foreach ($schedules4 as $schedule) : ?>
                        <tr>

                            <td><?php echo $schedule->name;?></td>

                        </tr>

                    <?php  endforeach;?>
                    </tbody>
                </table>
            </div>
        </div>
        <!--        end-->



    </div>
    <div>

        <div>
            <div>
                <form action="" method="POST">
                    <div class="form-gorup">
                        <?php
                        $subjects = Subject::find_all();

                        if(isset($_GET['c_id']) && isset($_GET['day_id']) && isset($_GET['p_id'])){

                            echo "<label>Subject</label><br>";
                            echo "<select multiple class='form-group' style='height: 150px;width: 200px;  '  name='subject_id'><br>";

                            foreach ($subjects as $subject){
                                echo "<option value=".$subject->id .">" .$subject->name . "</option>";
                            }
                            echo "</select><br>";

                            echo "<div class='form-gorup'>";
                            echo "<input type='submit' name='submit' class='btn btn-primary' value='Change'><br>";
                            echo "</div>";

                        }

                        ?>

                    </div>

                </form>
            </div>
        </div>
    </div>





    <div>











        <!-- /.container-fluid -->

        <!-- Sticky Footer -->



<?php  include ('includes/footer.php') ?>