<?php
$final_grade = new Final_grade();

// $subject_id = 4;
//
// $final_grade = Final_grade::show_grade($subject_id);
?>
<script>

am4core.useTheme(am4themes_animated);

var chart = am4core.create("chartdiv", am4charts.XYChart);


chart.colors.saturation = 0.4;

chart.data = [{
	"subject": "Matematika",
	"average": <?php Final_grade::list_final_grades(1); ?>
}, {
	"subject": "Srpski",
	"average": <?php Final_grade::list_final_grades(2); ?>
}, {
	"subject": "Fizicko",
	"average": <?php Final_grade::list_final_grades(3); ?>
}, {
	"subject": "Tehnicko",
	"average": <?php Final_grade::list_final_grades(4); ?>
}, {
	"subject": "Likovno",
	"average": <?php Final_grade::list_final_grades(5); ?>
}, {
	"subject": "Muzicko",
	"average": <?php Final_grade::list_final_grades(6); ?>
}, {
	"subject": "Biologija",
	"average": <?php Final_grade::list_final_grades(7); ?>
}, {
	"subject": "Engleski",
	"average": <?php Final_grade::list_final_grades(8); ?>
}, {
	"subject": "Fizika",
	"average": <?php Final_grade::list_final_grades(9); ?>
}, {
	"subject": "Hemija",
	"average": <?php Final_grade::list_final_grades(10); ?>

}];


var categoryAxis = chart.yAxes.push(new am4charts.CategoryAxis());
categoryAxis.renderer.grid.template.location = 0;
categoryAxis.dataFields.category = "subject";
categoryAxis.renderer.minGridDistance = 20;

var valueAxis = chart.xAxes.push(new am4charts.ValueAxis());
valueAxis.renderer.maxLabelPosition = 0.98;

var series = chart.series.push(new am4charts.ColumnSeries());
series.dataFields.categoryY = "subject";
series.dataFields.valueX = "average";
series.tooltipText = "{valueX.value}";
series.sequencedInterpolation = true;
series.defaultState.transitionDuration = 1000;
series.sequencedInterpolationDelay = 100;
series.columns.template.strokeOpacity = 0;

chart.cursor = new am4charts.XYCursor();
chart.cursor.behavior = "panY";


// as by default columns of the same series are of the same color, we add adapter which takes colors from chart.colors color set
series.columns.template.adapter.add("fill", function (fill, target) {
	return chart.colors.getIndex(target.dataItem.index);
});
</script>
