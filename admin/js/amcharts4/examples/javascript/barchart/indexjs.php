<?php include("../includes/init.php"); ?>
<script>
am4core.useTheme(am4themes_animated);

var chart = am4core.create("chartdiv", am4charts.XYChart);


chart.colors.saturation = 0.4;

chart.data = [{
	<?php echo ("subject :")?> "USA",
	"average": <?php echo 1; ?>
}, {
	"subject": "China",
	"average": <?php echo 1; ?>
}, {
	"subject": "Japan",
	"average": <?php echo 2; ?>
}, {
	"subject": "Germany",
	"average": <?php echo 1; ?>
}, {
	"subject": "UK",
	"average": <?php echo 1; ?>
}, {
	"subject": "France",
	"average": <?php echo 1; ?>
}, {
	"subject": "India",
	"average": <?php echo 1; ?>
}, {
	"subject": "Spain",
	"average": <?php echo 1; ?>
}, {
	"subject": "Netherlands",
	"average": <?php echo 2; ?>
}, {
	"subject": "Russia",
	"average": <?php echo 3; ?>
}, {
	"subject": "South Korea",
	"average": <?php echo 4; ?>
}, {
	"subject": "Canada",
	"average": <?php echo 5; ?>
}];


var categoryAxis = chart.yAxes.push(new am4charts.CategoryAxis());
categoryAxis.renderer.grid.template.location = 0;
categoryAxis.dataFields.category = "subject";
categoryAxis.renderer.minGridDistance = 20;

var valueAxis = chart.xAxes.push(new am4charts.ValueAxis());
valueAxis.renderer.maxLabelPosition = 0.98;

var series = chart.series.push(new am4charts.ColumnSeries());
series.dataFields.categoryY = "subject";
series.dataFields.valueX = "average";
series.tooltipText = "{valueX.value}";
series.sequencedInterpolation = true;
series.defaultState.transitionDuration = 1000;
series.sequencedInterpolationDelay = 100;
series.columns.template.strokeOpacity = 0;

chart.cursor = new am4charts.XYCursor();
chart.cursor.behavior = "panY";


// as by default columns of the same series are of the same color, we add adapter which takes colors from chart.colors color set
series.columns.template.adapter.add("fill", function (fill, target) {
	return chart.colors.getIndex(target.dataItem.index);
});
</script>
