<?php include ('includes/header.php')?>
<?php if(!$session->is_singed_in()) {redirect("login.php");}?>
<?php
if($session->role_id !=='1'){

    check_role($session->role_id);

}
$subjects = Subject::find_all();

$schedule = new Schedule();
$schedule2 = new Schedule();
$schedule3 = new Schedule();
$schedule4 = new Schedule();
$schedule5 = new Schedule();
$schedule6 = new Schedule();

if(isset($_POST['create'])){
    if($_POST['subject_name1'] == 'default' || $_POST['subject_name2'] == 'default' || $_POST['subject_name3'] == 'default' || $_POST['subject_name4'] == 'default' || $_POST['subject_name5'] == 'default' || $_POST['subject_name6'] == 'default'){
        $session->message("Please select subject for all periods");
        redirect("schedule_form.php?c_id=".$_GET['c_id']."&day_id=".$_GET['day_id']. "");
    } else {
        if($schedule){
            $schedule->day_id = $_GET['day_id'];
            $schedule->period_id = $_POST['p1'];
            $schedule->subject_id = $_POST['subject_name1'];
            $schedule->class_id = $_GET['c_id'];
            $schedule->save();

        }
        if($schedule2){
            $schedule2->day_id = $_GET['day_id'];
            $schedule2->period_id = $_POST['p2'];
            $schedule2->subject_id = $_POST['subject_name2'];
            $schedule2->class_id = $_GET['c_id'];
            $schedule2->save();
        }
        if($schedule3){
            $schedule3->day_id = $_GET['day_id'];
            $schedule3->period_id = $_POST['p3'];
            $schedule3->subject_id = $_POST['subject_name3'];
            $schedule3->class_id = $_GET['c_id'];
            $schedule3->save();
        }
        if($schedule4){
            $schedule4->day_id = $_GET['day_id'];
            $schedule4->period_id = $_POST['p4'];
            $schedule4->subject_id = $_POST['subject_name4'];
            $schedule4->class_id = $_GET['c_id'];
            $schedule4->save();
        }
        if($schedule5){
            $schedule5->day_id = $_GET['day_id'];
            $schedule5->period_id = $_POST['p5'];
            $schedule5->subject_id = $_POST['subject_name5'];
            $schedule5->class_id = $_GET['c_id'];
            $schedule5->save();
        }
        if($schedule6){
            $schedule6->day_id = $_GET['day_id'];
            $schedule6->period_id = $_POST['p6'];
            $schedule6->subject_id = $_POST['subject_name6'];
            $schedule6->class_id = $_GET['c_id'];
            $schedule6->save();
        }
         redirect("make_schedule.php?c_id=".$_GET['c_id']."");
    }


}





?>

    <body id="page-top">
    <!-- Top Nav -->
<?php include ('includes/top_nav.php')?>

<div id="wrapper">

    <!-- Sidebar -->
<?php include ('includes/sidebar.php')?>

<div id="content-wrapper">
    <div class="container-fluid">
        <!-- Breadcrumbs-->

        <ol class="breadcrumb">
            <li class="breadcrumb-item">
                <a href="index.php">Admin Dashboard</a>
            </li>
            <li class="breadcrumb-item active">Overview</li>
        </ol>

        <div class="col-md-6">
            <p class="bg-warning">
                <?php echo $message; ?>
            </p>
            <form action="" method="POST">
                <div class="form-group">
                <label for="period">Period 1</label>
                <input type="number" name="p1" value="1" hidden="true">
                <select name="subject_name1" class="browser-default custom-select">
                    <option value="default">Select Subject</option>
                    <?php foreach($subjects as $subject) : ?>
                    <option value="<?php echo $subject->id;?>"><?php echo $subject->name;?></option>
                    <?php endforeach;?>
                </select>
                </div>
                <div class="form-group">
                    <label for="period">Period 2</label>
                    <input type="number" name="p2"  value="2" hidden="true">
                    <select name="subject_name2" class="browser-default custom-select">
                        <option value="default">Select Subject</option>
                        <?php foreach($subjects as $subject) : ?>
                            <option value="<?php echo $subject->id;?>"><?php echo $subject->name;?></option>
                        <?php endforeach;?>
                    </select>
                </div>
                <div class="form-group">
                    <label for="period">Period 3</label>
                    <input type="number" name="p3"  value="3" hidden="true">
                    <select name="subject_name3" class="browser-default custom-select">
                        <option value="default">Select Subject</option>
                        <?php foreach($subjects as $subject) : ?>
                            <option value="<?php echo $subject->id;?>"><?php echo $subject->name;?></option>
                        <?php endforeach;?>
                    </select>
                </div>
                <div class="form-group">
                    <label for="period">Period 4</label>
                    <input type="number" name="p4"  value="4" hidden="true">
                    <select name="subject_name4" class="browser-default custom-select">
                        <option value="default">Select Subject</option>
                        <?php foreach($subjects as $subject) : ?>
                            <option value="<?php echo $subject->id;?>"><?php echo $subject->name;?></option>
                        <?php endforeach;?>
                    </select>
                </div>
                <div class="form-group">
                    <label for="period">Period 5</label>
                    <input type="number" name="p5"  value="5" hidden="true">
                    <select name="subject_name5" class="browser-default custom-select">
                        <option value="default">Select Subject</option>
                        <?php foreach($subjects as $subject) : ?>
                            <option value="<?php echo $subject->id;?>"><?php echo $subject->name;?></option>
                        <?php endforeach;?>
                    </select>
                </div>
                <div class="form-group">
                    <label for="period">Period 6</label>
                    <input type="number" name="p6"  value="6" hidden="true">
                    <select name="subject_name6" class="browser-default custom-select">
                        <option value="default">Select Subject</option>
                        <?php foreach($subjects as $subject) : ?>
                            <option value="<?php echo $subject->id;?>"><?php echo $subject->name;?></option>
                        <?php endforeach;?>
                    </select>
                </div>
                <div class="form-gorup">
                    <input type="submit" name="create" value="Create" class="btn btn-primary">
                </div>
            </form>
        </div>








    </div>

    <!-- /.container-fluid -->

    <!-- Sticky Footer -->

<?php  include ('includes/footer.php') ?>