<?php
class Open_door extends Db_object
{

    protected static $db_table = 'open_door';
    protected static $db_table_id = 'id';
    protected static $db_table_fields = array( 'visitor', 'host', 'term_id','confirm','date','status');
    public $visitor;
    public $host;
    public $term_id;
    public $confirm;
    public $id;
    public $date;
    public $status;




    public static function find_all_visitors($user_id_parent)
    {

        return static::find_by_query("SELECT * FROM " . static::$db_table . " where host = {$user_id_parent} and confirm = 'Pending' ORDER BY visitor DESC");

    }

    public static function update_open_door($con, $id){
        global $database;
        $query = "UPDATE ".static::$db_table." SET confirm =".$con." WHERE ".static::$db_table_id."=". $id;
        $database->query($query);

        return (mysqli_affected_rows($database->connection) == 1) ? true : false;
    }
    public static function find_by_confirmation($parent_id){
        return static::find_by_query("SELECT * FROM " . static::$db_table . " where visitor = {$parent_id} and (confirm = 'Yes' or confirm = 'No') ORDER BY id DESC LIMIT 5");
    }
    public static function info_of_confirmation($text, $hostF, $hostL, $term, $date){
        if($text == 'Yes'){
            echo "<br><h5><i style=\"color: green\"> Učitelj ".$hostF."  ".$hostL." prihvata Vaš dolazak u terminu:  ".$term."</i>, dana ".$date."</h5>";
        }else{
            echo "<br><h5><i style=\"color: red\"> Učitelj ".$hostF."  ".$hostL."  NE prihvata Vaš dolazak u terminu:  ".$term."</i>, dana ".$date."</h5>";
        }
    }

    public static function count_open_door(){
        global $database;
        $sql="SELECT * FROM open_door WHERE status = 0 and (confirm = 'Yes' or confirm = 'No')";
        $result= $database->query($sql);
        $count = mysqli_num_rows($result);
        return $count;
    }
    public static function set_zero(){
        global $database;
        $sql="UPDATE open_door SET status=1 WHERE status=0 and (confirm = 'Yes' or confirm = 'No')";
        $result = $database->query($sql);
        return $result;
    }


}
