<?php

class Schedule extends Db_object {

    protected static $db_table = 'schedule';
    protected static $db_table_id = 'id';
    protected static $db_table_fields = array('day_id','period_id','subject_id','class_id');

    public $id;
    public $day_id;
    public $period_id;
    public $subject_id;
    public $class_id;
    public $name;
    public $period_number;


    public static function show_schedule_by_day($class_id,$day_id)  {

        return self::find_by_query("SELECT sch.id, p.period_number, s.name from schedule as sch
                                        JOIN period as p on sch.period_id=p.id
                                        JOIN subjects as s on sch.subject_id = s.id
                                        WHERE sch.class_id = $class_id and sch.day_id = $day_id
                                        ORDER BY p.period_number asc");


    }

    public static function find_by_class($id,$day_id){
        global $database;
        $sql = "SELECT * FROM schedule WHERE class_id = $id AND day_id= $day_id";
        $result = $database->query($sql);
        $result_set = mysqli_fetch_assoc($result);
        return $result_set;


    }

    public static  function check_period($id,$day_id){
        global $database;
        $sql = "SELECT period_id FROM schedule where class_id= $id and day_id=$day_id";
        $result = $database->query($sql);
        $result_set = mysqli_fetch_array($result);
        return $result_set;


    }

    public static  function  check_periods($id,$day_id){

        return self::find_by_query("SELECT period_id FROM schedule where class_id= $id and day_id=$day_id");

    }




}// end of class Schedule






?>