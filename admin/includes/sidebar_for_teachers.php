<?php
$class1 = Departmant::find_class_by_user_id($session->user_id);
?>
<ul class="sidebar navbar-nav">
    <li class="nav-item dropdown">
        <a class="nav-link" href="../admin/view_students.php" role="button">
            <i class="fas fa-fw fa-folder"></i>
            <span>Spisak učenika</span></a>

    </li>
    <li class="nav-item dropdown">
        <a class="nav-link dropdown-toggle" href="#" id="pagesDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
            <i class="fas fa-fw fa-folder"></i>
            <span>Vaše poruke...</span>

        </a>
        <div class="dropdown-menu" aria-labelledby="pagesDropdown">
            <!-- <h6 class="dropdown-header">Login Screens:</h6> -->
            <?php
            foreach($parent_list as $parent){

                echo "<a class='dropdown-item'  href=./teacher_messages?parent_id=". $parent->id  .">".$parent->first_name." ".$parent->last_name."</a>";
            }
            ?>
        </div>
    </li>

    <li class="nav-item dropdown">
        <a class="nav-link" href="../admin/meeting.php" role="button">
            <i class="fas fa-fw fa-folder"></i>
            <span>Otvorena vrata</span></a>

    </li>
    <li class="nav-item dropdown">
            <a class="nav-link" href="view_schedule_by_class.php?c_id=<?php echo $class1->id;?>" role="button">
            <i class="fas fa-fw fa-folder"></i>
            <span>Raspored časova</span></a>

    </li>

</ul>
