<?php

class Teacher_schedule extends Db_object
{
    public static $db_table = "schedule";
    public static $db_table_id = "id";
    protected static $db_table_fields = array('day_id', 'period_id', 'subject_id', 'day_name', 'name');
    public $day_id;
    public $period_id;
    public $subject_id;
    public $day_name;
    public $name;



}