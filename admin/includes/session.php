<?php 

class Session{

	private $singed_id = false;
	public  $user_id;
	public  $role_id;
    public  $first_name;
    public  $last_name;
//	public  $count;
	public  $message;
	


	function __construct(){
		session_start();
		// $this->visitor_count();
		$this->check_the_login();
		$this->check_message();
		

	} //  End of construct


	public function message($msg=""){

		if(!empty($msg)){

			$_SESSION['message'] = $msg;

		} else {

			return $this->message;

		}
	}





	private function check_message(){

		if(isset($_SESSION['message'])){

			$this->message = $_SESSION['message'];
			unset($_SESSION['message']);

		} else {

			$this->message = "";
		}



	}




	// public function visitor_count(){
	// 	if(isset($_SESSION['count'])){
	// 		return $this->count = $_SESSION['count']++;
	// 	} else {
	// 		return $_SESSION['count'] = 1;
	// 	}
	// }


	public function is_singed_in(){
		return $this->singed_id;							// <---- This function checks if the user is singed in.
	}


	public function login($user){
		if($user){
			$this->user_id = $_SESSION['user_id'] = $user->id;     // <---- This function logs in the user.
            $this->role_id = $_SESSION['role_id'] = $user->role_id;
            $this->first_name = $_SESSION['first_name'] = $user->first_name;
            $this->last_name = $_SESSION['last_name'] = $user->last_name;
			$this->singed_id = true;
		}
	}

	public function logout(){
		unset($_SESSION['user_id']);
		unset($this->user_id);
        unset($this->role_id);
        unset($this->first_name);
        unset($this->last_name);
		$this->singed_id = false;
	}



	private function check_the_login(){
		if(isset($_SESSION['user_id'])){
			$this->user_id = $_SESSION['user_id'];
			$this->role_id = $_SESSION['role_id'];
			$this->first_name = $_SESSION['first_name'];
            $this->last_name = $_SESSION['last_name'];

			$this->singed_id = true;
		} else {
			unset($this->user_id);
			$this->singed_id = false;
		}
	}





}	// End of class Session

$session = new Session();
$message = $session->message();





 ?>