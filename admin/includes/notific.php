<?php
/**
 * Created by PhpStorm.
 * User: Grupa1
 * Date: 25/04/2019
 * Time: 12:01
 */

class Notifications extends Db_object
{
    public static $db_table = 'notifications';
    public static  $db_table_id = 'notifications_id';

    protected static $db_table_fields = array('notif_text','notif_title','created_time');
    public $notif_title;
    public $notif_text;
    public $created_time;
    public $status;

    public static function show_notification()
    {
        $not = "SELECT created_time, notif_title, notif_text FROM " . static::$db_table . " order by created_time desc limit 5";
        return self::find_by_query($not);
    }
    public static function update_notification(){
        global $database;
       $sql = "update ". static::$db_table ." set status = 1 where status = 0";
        $database->query($sql);
        return mysqli_affected_rows($database->connection);
    }
    public static function count_unseen(){
        $w = "select * from ". static::$db_table ." where status = 0";
        return sizeof(self::find_by_query($w) );

    }

}