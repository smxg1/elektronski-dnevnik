  <div class="col-md-6"> <!--aform start -->
    <form action="" method="POST" id="aform" name="aform" class="form">
      <div class="form-group">
        <label for="first_name">First Name: </label>
        <input type="text" name="first_name" class="form-control">
      </div>
      <div class="form-group">
        <label for="last_name">Last Name: </label>
        <input type="text" name="last_name" class="form-control">
      </div>
       <div class="form-group">
        <label for="username">Username: </label>
        <input type="text" name="username" class="form-control">
      </div>
       <div class="form-group">
        <label for="password">Password: </label>
        <input type="password" name="password" class="form-control">
      </div>
      <select name="role_id" class="role_id">
        <option value="1">Administrator</option>
      </select>
      <input type="submit" name="aform" value="Add User" class="btn btn-primary">
      <p><br></p>
    </form>
  </div><!-- aform end -->








  <div class="col-md-6"> <!--dform start -->
      <form action="" method="POST" id="dform" class="form">
        <div class="form-group">
          <label for="first_name">First Name: </label>
          <input type="text" name="first_name" class="form-control">
        </div>
        <div class="form-group">
          <label for="lastname">Last Name: </label>
          <input type="text" name="last_name" class="form-control">
        </div>
         <div class="form-group">
          <label for="username">Username: </label>
          <input type="text" name="username" class="form-control">
        </div>
         <div class="form-group">
          <label for="password">Password: </label>
          <input type="password" name="password" class="form-control">
        </div>
        <select name="role_id" class="role_id">
          <option value="2">Director</option>
        </select>
        <input type="submit" name="dform" value="Add User" class="btn btn-primary">
        <p><br></p>
      </form>
    </div><!-- dform end -->










  <div class="col-md-6"> <!-- tform start -->

      <form action="" method="POST" id="tform" class="form">


        <div class="form-group">

          <label for="first_name">First Name: </label>

          <input type="text" name="first_name" class="form-control">

        </div>


        <div class="form-group">

          <label for="lastname">Last Name: </label>

          <input type="text" name="last_name" class="form-control">

        </div>


        <div class="form-group">

         <label for="username">Username: </label>

         <input type="text" name="username" class="form-control">

       </div>


        <div class="form-group">

         <label for="password">Password: </label>

         <input type="password" name="password" class="form-control">

       </div>


       <div class="form-group">

         <label for="classes">Insert Class: </label>

         <input type="text" name="insert_class" class="form-control">

       </div>


       <select name="role_id" class="role_id">

         <option value="3">Teacher</option>

       </select>

       <input type="submit" name="tform" value="Add User" class="btn btn-primary">

       <p><br></p>


      </form>
    </div><!-- tform end -->













    <div class="col-md-6" > <!-- pform start -->

        <form action="" method="POST" id="pform" class="form">


          <div class="form-group">

            <label for="first_name">First Name: </label>

            <input type="text" name="first_name" class="form-control">

          </div>


          <div class="form-group">

            <label for="lastname">Last Name: </label>

            <input type="text" name="last_name" class="form-control">

          </div>


           <div class="form-group">

            <label for="username">Username: </label>

            <input type="text" name="username" class="form-control">

          </div>


           <div class="form-group">

            <label for="password">Password: </label>

            <input type="password" name="password" class="form-control">

          </div>


          <div class="form-group">

            <label for="student_name">Student Name: </label>

            <input type="text" name="student_name" class="form-control">

          </div>


          <div class="form-group">

            <label for="class_name">Class Name</label>

            <select name="class_name">

              <option selected="selected">Choose one</option>

              <?php


              $classes = Classes::find_all();

              foreach($classes as $class) : { ?>

                <option  value="<?php echo $class->id; ?>"><?php echo $class->name; ?></option>

                <?php


              } endforeach; ?>

            </select>

          </div>


          <select name="role_id" class="role_id">

            <option value="4">Parent</option>

          </select>

          <input type="submit" name="pform" value="Add User" class="btn btn-primary">

          <p><br></p>

        </form>

      </div><!-- pform end -->













      <div class="form-gorup" ><!-- role form start -->
        <form action="" method="POST">
        <label for="role">User Role</label>
        <select name="role" id="role">
          <option value="0">- Choose Option -</option>
          <option value="1">Administrator</option>
          <option value="2">Director</option>
          <option value="3">Teacher</option>
          <option value="4">Parent</option>
        </select>
      </div><!-- role form end -->
      <div class="form-gorup">
      </div>


    </form>
