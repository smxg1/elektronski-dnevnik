<?php
class Photos extends Db_object{
    public static $db_table = 'photos';
    public static $db_table_id = 'id';
    protected static $db_table_fields = array('photo_src','user_id','photo_name');
    public $photo_src;
    public $photo_name;
    public $id;
    public $user_id;


    public static function get_photo($session_id){
        return self::find_by_query("select * from photos where user_id = {$session_id}");
    }

}  //end of class Photos