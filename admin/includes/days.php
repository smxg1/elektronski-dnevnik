<?php

class Days extends Db_object{

    protected static $db_table = 'days';
    protected static $db_table_id = 'id';
    protected static $db_table_fields = array('day_name');

    public $id;
    public $day_name;
    public $ponedeljak = 1;
    public $utorak = 2;
    public $sreda = 3;
    public $cetvrtak = 4;
    public $petak = 5;


    public static function find_days_id(){
        global $database;
        $sql = "SELECT id FROM days";
        $result = $database->query($sql);
        $result_set = mysqli_fetch_assoc($result);
        return $result_set;


    }


} //  End of class Days


?>