<?php
class Main_photos extends Db_object{
    public static $db_table = 'main_photos';
    public static $db_table_id = 'id';
    protected static $db_table_fields = array('photo_id','user_id');
    public $photo_id;
    public $user_id;
    public $photo_src;
    public static function show_main_photo($session_id){
        return self::find_by_query("select photos.photo_src, main_photos.photo_id from photos left join
                                        main_photos on photos.id = main_photos.photo_id where main_photos.user_id={$session_id} LIMIT 1 ");
    }
    public static function find_main_photo($session_id){
        return self::find_by_query("select * from main_photos where user_id={$session_id} LIMIT 1 ");
    }
    public static function update_main_photo($session_id, $photo_id){
        global $database;
        $sql="UPDATE main_photos SET photo_id={$photo_id} WHERE user_id={$session_id}";
        $result = $database->query($sql);
        return $result;
    }

}  //end of class Photos