<?php
include ('includes/init.php');
$count=0;
$count = Notification::count_noti();

?>
<script type="text/javascript">

    function myFunction() {
        $.ajax({
            url: "./get_notif_for_teacher.php",
            type: "POST",
            processData:false,
            success: function(data){
                $("#notification-count").remove();
                $("#notification-latest").show();
                $("#notification-latest").html(data);
            },
            error: function(){}
        });
    }

    $(document).ready(function() {
        $('body').click(function(e){
            if ( e.target.id != 'notification-icon'){
                $("#notification-latest").hide();
            }
        });
    });


</script>
<nav class="navbar navbar-expand navbar-dark bg-dark static-top">

    <a class="navbar-brand mr-1" href="index.php">Dnevnik</a>

    <button class="btn btn-link btn-sm text-white order-1 order-sm-0" id="sidebarToggle" href="#">
        <i class="fas fa-bars"></i>
    </button>
    <!-- Navbar -->
    <ul class="navbar-nav ml-auto ml-md-12">
        <li class="nav-item dropdown no-arrow mx-1">
            <a class="nav-link dropdown-toggle" href="#" id="notification-icon" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"  name="button" onclick="myFunction()">
                <span class="badge badge-danger" id="notification-count"><?php if($count>0) { echo $count; } ?></span>
                <i class="fas fa-bell fa-fw"></i>
            </a>
<<<<<<< HEAD
            <div class="dropdown-menu dropdown-menu-right" aria-labelledby="alertsDropdown" id="notification-latest">
=======
            <div class="dropdown-menu dropdown-menu-right" aria-labelledby="alertsDropdown">
                <a class="dropdown-item" href="#">Vaša obaveštenja</a>
                <a class="dropdown-item" href="#">Another action</a>
                <div class="dropdown-divider"></div>
                <a class="dropdown-item" href="#">Something else here</a>
>>>>>>> a2413f5e4185c94eca632f54ff30c269bb5dc689
            </div>
        </li>
        <li class="nav-item dropdown no-arrow mx-1">
            <a class="nav-link dropdown-toggle" href="#" id="messagesDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                <i class="fas fa-envelope fa-fw"></i>
                <span class="badge badge-danger">7</span>
            </a>
            <div class="dropdown-menu dropdown-menu-right" aria-labelledby="messagesDropdown">
                <a class="dropdown-item" href="#">Pošalji poruku..</a>
                <a class="dropdown-item" href="#">Another action</a>
                <div class="dropdown-divider"></div>
                <a class="dropdown-item" href="#">Something else here</a>
            </div>
        </li>
        <li class="nav-item dropdown no-arrow">
            <a class="nav-link dropdown-toggle" href="#" id="userDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                <i class="fas fa-user-circle fa-fw"></i>
            </a>
            <div class="dropdown-menu dropdown-menu-right" aria-labelledby="userDropdown">
                <a class="dropdown-item" href="#">Settings</a>
                <a class="dropdown-item" href="./account.php"><?php echo $session->first_name." ". $session->last_name?></a>
                <div class="dropdown-divider"></div>
                <a class="dropdown-item" href="./logout.php">Logout</a>
            </div>
        </li>
    </ul>

</nav>