<?php
include ('includes/header.php');
if(!$session->is_singed_in()) {redirect("login.php");}
//$grades = Grade::show_grades_to_parent($session->user_id);

//$student_name=$grades[0]->student_name;
//$teacher_class=Grade::show_teacher_class($student_name);
$parent_list = User::list_parents();
//var_dump($parent_list);

?>
<body id="page-top">
<!-- Top Nav -->
<!-- Sidebar -->
<?php
if($session->role_id == 3){
    include 'includes/top_nav_for_teachers.php';
    echo '<div id="wrapper">';
    include 'includes/sidebar_for_teachers.php';
}else{
    include ('includes/sidebar.php');
    echo '<div id="wrapper">';
    include ('includes/top_nav.php');
}
?>
<div id="content-wrapper">
    <nav class="container-fluid">
        <!-- Breadcrumbs-->
        <ol class="breadcrumb">
            <li class="breadcrumb-item">
                <a href="#">Poruke</a>
            </li>
            <li class="breadcrumb-item active">
                </li>
        </ol>


        <!-- mesto za izlistavanje poruka -->
        <?php
        if(isset($_GET['parent_id'])){
            $parent_id = $_GET['parent_id'];
            $items_per_page = 3;
            $total_pages = num_of_pages(Messages::total($session->user_id, $parent_id),$items_per_page);

            isset($_GET['page']) ? $page = $_GET['page'] : $page = $total_pages;

                if($page == 0)
                    $page = 1;
            $offset = ($page-1)*$items_per_page;   //rows in the table to escape
            $messages = Messages::list_messages($session->user_id, $parent_id, $items_per_page, $offset);

            echo "<div class=\"container\"><ul class=\"pagination\">";
            for($page=1; $page<=$total_pages; $page++){
                echo "<li class='page-item'>
                           <a class='page-link' href='teacher_messages.php?parent_id=".$parent_id."&page=".$page."'>{$page}
                           </a></li>";
            }
            echo "</ul></div>";

            if(isset($_GET['message_id'])){
                $mess1 = Messages::find_by_id($_GET['message_id']);
                $mess1->id = $_GET['message_id'];
                if($mess1)
                    $mess1->delete();
                // $session->message("good");
                redirect($_SERVER['HTTP_REFERER']);
            }
                foreach ($messages as $message){
                    if ($message->sender == $session->user_id){
                        if ($message->receiver == $parent_id) {
                            echo " Vi ( " . $session->first_name . " ), kome ( " . User::get_user($message->receiver)[0]->first_name . "):<p class=\"text-black-50\"> " . $message->message_content . "</p>";
                        }
                    } elseif ($message->sender == $teacher_id) {
                        echo " ( " . User::get_user($message->sender)[0]->first_name . " ): <p class=\"text-black-50\">" . $message->message_content . "</p>";
                    }
                    echo "<i>Datum: ". $message->date_time . "</i>";
                    echo "<a href='teacher_messages.php?parent_id=".$parent_id."&page=".$page."&message_id=".$message->id."' onclick=\"return confirm('Da li ste sigurni da želite da obrišete poruku?')\"><img src='pictures/delete-message.png' width='30px' height='30'></a>";
                    echo "<div class=\"dropdown-divider\"></div>";
                }


        }

        $newMessage = new Messages;
        if(isset($_POST['submit'])){

                if ($newMessage) {
                    $newMessage->message_content = $_POST['message_content'];
                    $newMessage->sender = $session->user_id;
                    $newMessage->receiver = $_GET['parent_id'];
                    $newMessage->save();
                    redirect("teacher_messages.php?parent_id={$newMessage->receiver}");
                }
            }

        ?>
        <!-- mesto za slanje poruke -->
        <div >
            <form action="" method="POST">

                <div class="form-group">
                    <label for="message_content">Sadrzaj poruke:</label>
                    <textarea class="form-control" rows="5" name="message_content" placeholder="tekst poruke.."></textarea>
                </div>
                <div class="form-group">

                </div>
                <button type="submit" name="submit" class="btn btn-secondary" >Pošalji</button>
            </form>
        </div>
        <!-- /.container-fluid -->
        <!-- Sticky Footer -->
        <?php  include ('includes/footer.php') ?>
