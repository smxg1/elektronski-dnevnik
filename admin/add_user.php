<?php include('includes/header.php');?>
<?php if(!$session->is_singed_in()) {redirect("login.php");}?>
<?php

if($session->role_id !=='1'){
  $session->message("You don't have access to that page");
  check_role($session->role_id);

}



 ?>
<!-- Top Nav -->
<?php include('includes/top_nav.php');?>



<div id="wrapper">

    <!-- Sidebar -->
    <?php include('includes/sidebar.php');?>

    <div id="content-wrapper">
      <div class="container-fluid">

        <!-- Breadcrumbs-->

        <ol class="breadcrumb">
          <li class="breadcrumb-item">
            <a href="index.php">Admin Dashboard</a>
          </li>
          <li class="breadcrumb-item active">Add User</li>
        </ol>
        <p class="bg-warning">
             <?php echo $message; ?>
        </p>

          <?php include('includes/forms.php'); // all forms are here?>

      </div>

    </div>

</div>





<?php
$classes = new Classes();
$user = new User();
$student = new Student();


if(isset($_POST['aform']) || isset($_POST['dform']) || isset($_POST['tform']) || isset($_POST['pform'])){

  if($user){
    $user->first_name =  $_POST['first_name'];
    $user->last_name =  $_POST['last_name'];
    $user->username =  $_POST['username'];
    $user->password =  $_POST['password'];
    $user->role_id =  $_POST['role_id'];

      if ($user->first_name == '' || $user->last_name == '' || $user->username == '' || $user->password == '') {

              redirect("add_user.php");
              $session->message("You must fill all fields");

            } else {

              $user->save();
              $session->message("The user {$user->first_name} {$user->last_name} has been created");
              redirect('view_users.php');


            }

         if(isset($_POST['tform'])){

            $classes->name = $_POST['insert_class'];
            $classes->user_id = $database->the_insert_id();

            if ($classes->name == '' || $classes->user_id == '') {
                $session->message("You must fill all fields");
              } else {
            $classes->save();
          }}

          if(isset($_POST['pform'])) {

            $student->student_name = $_POST['student_name'];
            $student->class_id =  $_POST['class_name'];
            $student->user_id = $database->the_insert_id();

            if ($student->student_name == '' || $student->class_id == '' || $student->user_id == '') {
              $session->message("You must fill all fields");
              } else {
             $student->save();
           }}




    }

}







 ?>







<<<<<<< HEAD
    <div class="col-md-6">
        <form action="" method="POST" id="parent">
          <div class="form-group">
            <label for="first_name">First Name: </label>
            <input type="text" name="first_name" class="form-control">
          </div>
          <div class="form-group">
            <label for="lastname">Last Name: </label>
            <input type="text" name="last_name" class="form-control">
          </div>
           <div class="form-group">
            <label for="username">Username: </label>
            <input type="text" name="username" class="form-control">
          </div>
           <div class="form-group">
            <label for="password">Password: </label>
            <input type="password" name="password" class="form-control">
          </div>
          </form>
          
          <form>
          <div class="form-gorup">
            <label for="role_id">User Role</label>
            <select name="role_id">
              <option value="0">Choose Option</option>
              <option value="1">Administrator</option>
              <option value="2">Director</option>
              <option value="3">Teacher</option>
              <option value="4">Parent</option>              
            </select>
          </div>
          <div class="form-gorup">
            <input type="submit" name="create" value="Add User" class="btn btn-primary">
          </div>
   </div>


        </form>



      
=======
























        <script>


          $(document).ready(function(){


            $('#aform').hide();
            $('#dform').hide();
            $('#tform').hide();
            $('#pform').hide();
            $('.role_id').hide();



            $('#role').change(function(){


              if($('#role').val() == '1'){

                $('#aform').show(500);



              } else {

                $('#aform').hide(1000);

              }

              if($('#role').val() == '2'){

                $('#dform').show(500);



              } else {

                $('#dform').hide(1000);

              }

              if($('#role').val() == '3') {

                $('#tform').show(500);



              } else {

                $('#tform').hide(1000);

              }

              if($('#role').val() == '4') {

                $('#pform').show(1000);



              } else {

                $('#pform').hide(1000);

              }



            }); //change function end



        }); // document ready function end











        </script>



>>>>>>> 6d4688051fb9e5d5325f5f3e0e60082a25d02102
      <!-- /.container-fluid -->

      <!-- Sticky Footer -->

<?php  include ('includes/footer.php') ?>
