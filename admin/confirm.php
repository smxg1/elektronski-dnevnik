<?php
include ('includes/header.php');
if(!$session->is_singed_in()) {redirect("login.php");}

if($session->role_id !=='3'){
    $session->message("You don't have access to that page");
    check_role($session->role_id);
}

$edit_door = Open_door::find_by_id($_GET['open_door_id']);
$visitor_id = $_GET['visitor'];
$parent1 = User::get_user($visitor_id);
//var_dump($parent);
//die();
?>
    <!-- Top Nav -->
<?php include ('includes/top_nav_for_teachers.php');
?>
    <div id="wrapper">

    <!-- Sidebar -->
    <?php include ('includes/sidebar_for_teachers.php')?>

    <div id="content-wrapper">
        <div class="container-fluid">


            <!-- Breadcrumbs-->

            <ol class="breadcrumb">
                <li class="breadcrumb-item">
                    <a href="index.php">Teacher Dashboard</a>
                </li>
                <li class="breadcrumb-item active">  </li></li>
            </ol>

            <div class="col-md-6">
                <?php
                    if($edit_door){
                        $edit_door->confirm = 'Yes';
                        $edit_door->update();
                        $session->message("<i><h3 color='green'>Prihvatili ste zahtev za otvorena vrata roditelja ".$parent1[0]->first_name." ".$parent1[0]->last_name."</h3> datum". $edit_door->date."</i>");
                         redirect($_SERVER['HTTP_REFERER']);
                    }

                ?>

            </div>
        </div>


    </div>



    <!-- /.container-fluid -->

    <!-- Sticky Footer -->

<?php  include ('includes/footer.php') ?>