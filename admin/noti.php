
<?php
include ('includes/init.php');
$count=0;
// Create connection

// Check connection


$noti = new Notification();

if(isset($_POST["submit"]))
{
    if($noti){

        $noti->title = $_POST["title"];
        $noti->text =  $_POST["text"];
        $noti->insert_noti($noti->title,$noti->text);

    }


}

$count = Notification::count_noti();

?>

<html>
	<head>
		<meta charset="UTF-8" />
		<meta name="viewport" content="width=device-width, initial-scale=1">
		<title>Notification</title>
	<link rel="stylesheet" href="notification-demo-style.css" type="text/css">
	<script src="https://code.jquery.com/jquery-2.1.1.min.js" type="text/javascript"></script>
	
	
	</head>
	<body>
	<div class="demo-content">
		<div id="notification-header">
			   <div style="position:relative">
			   <button id="notification-icon" name="button" onclick="myFunction()" class="dropbtn"><span id="notification-count"><?php if($count>0) { echo $count; } ?></span><img src="notification-icon.png" /></button>
				 <div id="notification-latest"></div>
				</div>			
		</div>
	<?php if(isset($message)) { ?> <div class="error"><?php echo $message; ?></div> <?php } ?>


	<?php if(isset($success)) { ?> <div class="success"><?php echo $success;?></div> <?php } ?>

		<form name="frmNotification" id="frmNotification" action="" method="post" >
			<div id="form-header" class="form-row">Kreiraj obavijest</div><br>
			<div class="form-row">
				<div class="form-label">Naziv obavjestenja:</div><div class="error" id="title"></div>
				<div class="form-element">
					<input type="text"  name="title" id="title" required /> 
					
				</div>
			</div><br>
			<div class="form-row">
				<div class="form-label">Obavjestenje:</div><div class="error" id="text"></div>
				<div class="form-element">
					<textarea rows="4" cols="30" name="text" id="text"></textarea>
				</div>
			</div>
			
			
			<div class="form-row">
				<div class="form-element">
					<input type="submit" name="submit" id="submit" value="Submit"/>
				</div>
			</div>
</form>
		</div>
	</body>
</html>

<script type="text/javascript">

	function myFunction() {
		$.ajax({
			url: "view_notification.php",
			type: "POST",
			processData:false,
			success: function(data){
				$("#notification-count").remove();					
				$("#notification-latest").show();$("#notification-latest").html(data);
			},
			error: function(){}           
		});
	 }
	 
	 $(document).ready(function() {
		$('body').click(function(e){
			if ( e.target.id != 'notification-icon'){
				$("#notification-latest").hide();
			}
		});
	});
		 
	</script>



