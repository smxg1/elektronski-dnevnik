<?php
include ('includes/header.php');
if(!$session->is_singed_in()) {redirect("login.php");}
$grades = Grades::show_grades_by_id($session->user_id);
$student_name=$grades[0]->student_name;
$teacher_class=Grades::show_teacher_class($student_name);
$teacher_list = User::list_teachers();


?>
    <body id="page-top">



    <!-- Top Nav -->
    <!-- Sidebar -->
    <?php
        if($session->role_id == 4){
            include 'includes/top_nav_for_parents.php';
            echo '<div id="wrapper">';
            include 'includes/sidebar_for_parents.php';
        }else{
            include ('includes/sidebar.php');
            echo '<div id="wrapper">';
            include ('includes/top_nav.php');
        }
    ?>
<div id="content-wrapper">
    <div class="container-fluid">
    <!-- Breadcrumbs-->
    <ol class="breadcrumb">
        <li class="breadcrumb-item">
            <a href="index.php"><?php     echo $grades[0]->student_name;  ?></a>
        </li>
        <li class="breadcrumb-item active">
            <?php   echo "Učitelj:  ".$teacher_class[0]->first_name."  ".  $teacher_class[0]->last_name. " , ". $teacher_class[0]->name ?></li>
    </ol>
        <div id="content_notification"> </div>
    <!-- /.container-fluid -->

    <!-- Sticky Footer -->

<?php  include ('includes/footer.php') ?>