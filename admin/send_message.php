
<?php
include ('includes/header.php');
if(!$session->is_singed_in()) {redirect("login.php");}
$grades = Grade::show_grades_to_parent($session->user_id);
$student_name=$grades[0]->student_name;
$teacher_class=Grade::show_teacher_class($student_name);
$teacher_list = User::list_teachers();

?>
<body id="page-top">
<!-- Top Nav -->
<!-- Sidebar -->
    <?php
            if($session->role_id == 4){
                include 'includes/top_nav_for_parents.php';
                echo '<div id="wrapper">';
                include 'includes/sidebar_for_parents.php';
            }else{
                include ('includes/sidebar.php');
                echo '<div id="wrapper">';
                include ('includes/top_nav.php');
            }
    ?>
    <div id="content-wrapper">
        <nav class="container-fluid">
            <!-- Breadcrumbs-->
            <ol class="breadcrumb">
                <li class="breadcrumb-item">
                    <a href="index.php"><?php  echo $grades[0]->student_name;  ?></a>
                </li>
                <li class="breadcrumb-item active">
                    <?php   echo "Učitelj:  ".$teacher_class[0]->first_name."  ".  $teacher_class[0]->last_name  . " , ". $teacher_class[0]->name   ?></li>
            </ol>



            <?php

            $newMessage = new Messages;
            if(isset($_POST['submit'])){
                if($_POST['teacher']!='Izaberite učitelja :'){
                    if ($newMessage) {

                        $newMessage->message_content = $_POST['message_content'];
                        $newMessage->sender = $session->user_id;
                        $newMessage->receiver = $_POST['teacher'];
                        $newMessage->save();
                        echo '<p style="color: green">Message has been sent ... check inbox...';

                    }
                }else{
                    echo '<p style="color: red">Molimo Vas izaberite nastavnika!</p>';
                }
            }


            ?>
            <!-- mesto za slanje poruke -->

                <form action="" method="POST">

                    <div class="form-group">
                    <label for="message_content">Sadrzaj poruke:</label>
                        <textarea class="form-control" rows="5" name="message_content" placeholder="tekst poruke.."></textarea>
                    </div>
                    <div class="form-group">
                    <select name="teacher">
                       <option>Izaberite učitelja : </option>
                        <?php
                            foreach($teacher_list as $teacher){
                               echo "<option  value='". $teacher->id  ."'>".$teacher->first_name." ".$teacher->last_name."</option>";
                           }
                        ?>
                   </select>
                    </div>
                    <button type="submit" name="submit" class="btn btn-secondary" >Pošalji</button>
                </form>
    </div>
            <!-- /.container-fluid -->
            <!-- Sticky Footer -->
            <?php  include ('includes/footer.php') ?>
