<?php include ('includes/header.php')?>
<?php if(!$session->is_singed_in()) {redirect("login.php");}?>
<?php
if($session->role_id !=='1'){

    check_role($session->role_id);

}

$days = new Days();
$c_id = $_GET['c_id'];





?>

    <body id="page-top">
    <!-- Top Nav -->
<?php include ('includes/top_nav.php')?>

<div id="wrapper">

    <!-- Sidebar -->
<?php include ('includes/sidebar.php')?>

    <div id="content-wrapper">

    <div class="container-fluid">


    <!-- Breadcrumbs-->

    <ol class="breadcrumb">
        <li class="breadcrumb-item">
            <a href="index.php">Admin Dashboard</a>
        </li>
        <li class="breadcrumb-item active">Overview</li>
    </ol>

    <div class="row">

        <div class="col-md-10">
            <div class="col-md-2 raspored">
                <table class="table table-bordered">
                    <thead>
                    <tr>

                        <th><a href="">Ponedeljak</a></th>
                    </tr>
                    </thead>
                    <tbody>
                    <?php
                    $schedules = Schedule::show_schedule_by_day($c_id,$days->ponedeljak);
                    foreach ($schedules as $schedule) : ?>
                        <tr>

                            <td><?php echo $schedule->name;?></td>

                        </tr>
                    <?php endforeach; ?>
                    <td><a class="btn btn-primary" href="make_schedule.php?day_id=<?php echo $days->ponedeljak;?>">Edit</a></td>
                    </tbody>
                </table>
            </div>
            <div class="col-md-2 raspored">
                <table class="table table-bordered">
                    <thead>
                    <tr>
                        <th><a href="">Utorak</a></th>
                    </tr>
                    </thead>
                    <tbody>
                    <?php
                    $schedules1 = Schedule::show_schedule_by_day($c_id,$days->utorak);
                    foreach ($schedules1 as $schedule) : ?>
                        <tr>

                            <td><?php echo $schedule->name;?></td>

                        </tr>
                    <?php endforeach; ?>
                    <td><a class="btn btn-primary" href="make_schedule.php?day_id=<?php echo $days->utorak;?>">Edit</a></td>
                    </tbody>
                </table>
            </div>
            <div class="col-md-2 raspored">
                <table class="table table-bordered">
                    <thead>
                    <tr>
                        <th><a href="">Sreda</a></th>
                    </tr>
                    </thead>
                    <tbody>
                    <?php
                    $schedules2 = Schedule::show_schedule_by_day($c_id,$days->sreda);
                    foreach ($schedules2 as $schedule) : ?>
                        <tr>

                            <td><?php echo $schedule->name;?></td>

                        </tr>
                    <?php endforeach; ?>
                    <td><a class="btn btn-primary" href="make_schedule.php?day_id=<?php echo $days->sreda;?>">Edit</a></td>
                    </tbody>
                </table>
            </div>
            <div class="col-md-2 raspored">
                <table class="table table-bordered">
                    <thead>
                    <tr>
                        <th><a href="">Cetvrtak</a></th>
                    </tr>
                    </thead>
                    <tbody>
                    <?php
                    $schedules3 = Schedule::show_schedule_by_day($c_id,$days->cetvrtak);
                    foreach ($schedules3 as $schedule) : ?>
                        <tr>

                            <td><?php echo $schedule->name;?></td>

                        </tr>
                    <?php endforeach; ?>
                    <td><a class="btn btn-primary" href="make_schedule.php?day_id=<?php echo $days->cetvrtak;?>">Edit</a></td>
                    </tbody>
                </table>
            </div>
            <div class="col-md-2 raspored">
                <table class="table table-bordered">
                    <thead>
                    <tr>
                        <th><a href="">Petak</a></th>
                    </tr>
                    </thead>
                    <tbody>
                    <?php
                    $schedules4 = Schedule::show_schedule_by_day($c_id,$days->petak);
                    foreach ($schedules4 as $schedule) : ?>
                        <tr>

                            <td><?php echo $schedule->name;?></td>

                        </tr>
                    <?php endforeach; ?>
                    <td><a class="btn btn-primary" href="make_schedule.php?c_id=<?php echo $c_id;?>&day_id=<?php echo $days->petak;?>">Edit</a></td>
                    </tbody>
                </table>
            </div>
        </div>
        <div class="col-md-2">
            <form action="" method="POST">
                <div class="form-gorup">
                    <select name="" id="">
                        <option value="">

                        </option>
                    </select>
                </div>
            </form>
        </div>
    </div>





    <div>

        <div class="container">
        </div>

        <!-- /.container-fluid -->

        <!-- Sticky Footer -->

<?php  include ('includes/footer.php') ?>