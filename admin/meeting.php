<?php include ('includes/header.php');
if(!$session->is_singed_in()) {redirect("login.php");}
if($session->role_id !=='3'){
    $session->message("You don't have access to that page");
    check_role($session->role_id);
}
$visitors = Open_door::find_all_visitors($session->user_id);
include ('includes/top_nav_for_teachers.php');
?>
    <!-- Top Nav -->
    <div id="wrapper">

    <!-- Sidebar -->
    <?php include ('includes/sidebar_for_teachers.php')?>
    <div id="content-wrapper">
        <div class="container-fluid">
            <!-- Breadcrumbs-->
            <ol class="breadcrumb">
                <li class="breadcrumb-item">
                    <a href="#">Otvorena vrata</a>
                </li>
                <li class="breadcrumb-item active"> </li>
            </ol>
            <div class="col-md-6">

                        <?php  foreach($visitors as $visitor) :  ?>
                            <?php
                            //$visitor1 = Open_door::find_all_visitors($visitor->host);
                            $list_of_parents = User::get_user($visitor->visitor); //ovo cuva id visitora
                            $term_infotext = Terms::find_by_id($visitor->term_id);
                            ?>

                                 <?php echo "<p class=\"text-black-50\"><i>". $list_of_parents[0]->first_name." ".$list_of_parents[0]->last_name." zahteva dolazak u terminu: " .$term_infotext->term."</i></p>"; ?>
                                 <?php echo "<a href=confirm.php?visitor={$visitor->visitor}&term_id={$visitor->term_id}&open_door_id={$visitor->id}><img src='pictures/sign-check-icon.png' width='30' height='30'></a>";?>
                                 <?php echo "<a href=reject.php?visitor={$visitor->visitor}&term_id={$visitor->term_id}&open_door_id={$visitor->id}><img src='pictures/refuse.jpg' width='30' height='30'></a>";?>

                            <div class="dropdown-divider"></div>
                        <?php endforeach;
                             if(isset($message)){ echo $message;}
                        ?>

            </div>
        </div>
    </div>
    <!-- /.container-fluid -->
    <!-- Sticky Footer -->
<?php  include ('includes/footer.php') ?>